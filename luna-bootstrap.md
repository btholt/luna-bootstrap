# Twitter Bootstrap
## and some Emmet too
## by Brian Holt

---

# Who is Brian Holt?
- Born in Helena, MT
- Moved to Bountiful, UT
- Attended BYU
- Used to be phlyogenetic researcher
- Lived in Italy
- Worked at Xango, Nu Skin, KSL.com

---

## My wife is attractive.
![Nikki](../img/personal/wife.jpg)

---

## My friends are weird.
![Friends](../img/personal/friends.jpg)

---

## My parents are nice.
![Parents](../img/personal/parents.jpg)

---

## My siblings torture me.
![Siblings](../img/personal/siblings.jpg)

---

# Emmet
## What the hell is Emmet?

- [Emmet.io](http://emmet.io)
- Formerly called Zen Coding
- Think HTML templating (like Jade) but rendered on the fly.

---

# Why?
- Doesn't need to be compiled or pre-processed.
- Prints out normal HTML, readable to anyone who reads HTML.
- The syntax is essentially HTML and CSS.
- It's an editor extension where you type your statement, hit tab, and it extends it out to the full HTML.
- ![Ain't no body got time for that!](../img/memes/newlanguage.jpg)

---

# How does it work?

-
	button.btn
	&lt;button class="btn">&lt;/button>

	section#hero
	&lt;section id="hero">&lt;/section>

	#top.col.col1-3
	&lt;div id="col" class= "col col1-3">&lt;/div>

	a[href="/static/img/lolcat.jpg"]
	&lt;a href="/static/img/lolcat.jpg">&lt;/a>

	h2{SMRT}
	&lt;h2>SMRT&lt;/h2>

---

![Coding God](../img/memes/codinggod.jpg)

---

	html:5
	&lt;!doctype html>
	&lt;html lang="en">
	&lt;head>
		&lt;meta charset="UTF-8">
		&lt;title>Document&lt;/title>
	&lt;/head>
	&lt;body>
		
	&lt;/body>
	&lt;/html>

	.textbody>p{I love lamp.}
	&lt;div class="textbody">
		&lt;p>I love lamp.&lt;/p>
	&lt;/div>

---

	section>p{First paragraph}+p{Second paragraph}
	&lt;section>
		&lt;p>First paragraph&lt;/p>
		&lt;p>Second paragraph&lt;/p>
	&lt;/section>

	.main_body>ul*2>li*3>lorem5
	&lt;div class="main_body">
		&lt;ul>
			&lt;li>Lorem ipsum dolor sit amet.&lt;/li>
			&lt;li>At ad perferendis necessitatibus quis.&lt;/li>
			&lt;li>Vitae ullam veniam natus. Voluptas.&lt;/li>
		&lt;/ul>
		&lt;ul>
			&lt;li>Lorem ipsum dolor sit amet.&lt;/li>
			&lt;li>Aut culpa rerum exercitationem conseqr.&lt;/li>
			&lt;li>Tempora maxime totam quidem similique?&lt;/li>
		&lt;/ul>
	&lt;/div>

---

![Got more complicated](../img/memes/gotmorecomplicated.jpg)

---

	html:5>link[href="bootstrap/css/bootstrap.min.css"]
	+nav.navbar>.navbar-inner>a.brand[href="#"]{Havanese}
	+ul.nav>li*4>a[href="#"]{Link $}^^^^.hero-unit.span9>
	h1{Havanese!}+p>lorem40^button.btn.btn-large.btn-primary
	{Learn More}^section.questions.span5>.accordion
	#not_important>.accordion-group*3>.accordion-heading>
	a.accordion-toggle[data-toggle="collapse" 
	data-parent="#not_important" `href="#collapse$"]`
	{Question #$}^#collapse$.accordion-body.collapse>
	.accordion-inner>lorem75^^^^^section.images.span12>
	ul.thumbnails>li.span3*4>a[href="#"]>img.img-rounded
	[src="img/luna$.jpg"]^h3.text-center{Luna $}+small>lorem25^^^^
	section.span14.textbody>.page-header>h1>{Puppies}+small
	{ They're awesome.}^^.well*3>lorem100^^script
	[src="http://code.jquery.com/jquery-1.9.1.min.js"]+script
	[src="bootstrap/js/bootstrap.min.js"]

---

![Wat](../img/memes/wat.gif)

---

# Bootstrap

---

# What is Bootstrap?

- Simplified, front-end development is three things: content(HTML), style(CSS), and behavior(JavaScript.)
- Bootstrap aims to take care of (or at least greatly aid) style and behavior, leaving you to do content.
- Provides many common functionalities like modals, buttons, grids, and good basic styling.

---

# How?

- Bootstrap mainly works by adding divs with classes. Careful, because this can lead to div soup, though sometimes it's inevitable.
- Bootstrap's JavaScript does function by itself, but will leverage jQuery if available (use jQuery; it's much better.)
- Documentation is very explanitory. It's easy to pick and choose elements to use.

---

# Resources

- [Emmet.io](http://emmet.io)
- [Emmet Cheat Sheet](http://docs.emmet.io/cheat-sheet/)
- [Bootstrap](http://twitter.github.com/bootstrap/index.html)
- Presentation, Emmet snippets, and sample available on my Bitbucket
- [bitbucket.org/btholt](https://bitbucket.org/btholt)

---

## Live Demo
